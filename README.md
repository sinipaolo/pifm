# Dependencies

- pulseaudio
- sox
- [fm_transmitter](https://github.com/markondej/fm_transmitter)

Follow [this tutorial](https://forums.raspberrypi.com/viewtopic.php?t=161944) up to 
sudo nano /usr/lib/udev/bluetooth

## Run

Usage:

```shell
./stream.sh {frequency in MHz}
```
